#!/bin/bash

####################################################################
## Generates a bash quote header like so, based on length of input.
####################################################################

function HEADER() {
    n=$((4 + ${#1}))
    while ((n--)); do echo -n "#"; done; echo

    echo \#\#  $1

    n=$((4 + ${#1}))
    while ((n--)); do echo -n "#"; done; echo
}

HEADER "$1"


## CITATION: http://www.tldp.org/LDP/abs/html/arithexp.html
## CITATION: http://stackoverflow.com/questions/5349718/how-can-i-repeat-a-character-in-bash

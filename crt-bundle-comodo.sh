#!/bin/bash

###########################################################################
## Creates a bundled ssl crt file from a Comodo ssl + trust chain package
##
## @param $1 the file name (no extension) of the domain specific crt file.
###########################################################################

destFile=$1.ssl-bundle.crt

# Write File Header to be fancy
bash-header.sh "CRT BUNDLE :: $destFile" > $destFile

# Concat domain and trust chain .crt(s)
crt-echo.sh $1.crt >> $destFile
crt-echo-comodo.sh >> $destFile

# Echo output for user validation
cat $destFile

#!/bin/bash

################################################################
## Installs the csr-tools scripts using linux `install` command
################################################################

installBase=/usr/local/bin

install bash-header.sh $installBase/bash-header.sh
install csr-gen.sh $installBase/csr-gen.sh
install crt-echo.sh $installBase/crt-echo.sh
install crt-echo-comodo.sh $installBase/crt-echo-comodo.sh

install crt-bundle-comodo.sh $installBase/crt-bundle-comodo.sh



## @citation https://igurublog.wordpress.com/library/script-installation-instructions/ 


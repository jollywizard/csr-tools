#!/bin/bash

######################################################################
## Uses OpenSSL to generates an RSA Key and a CSR for a target domain.
##
## * Files are output to a relative directory matching the domain name.
## * Files are named after the domain.
## * OpenSSL will prompt user for CSR information.
## * The key will only be generated if not present, 
##   so it is not lost accidentally through repeated use.
##
## @param $1 the domain name to be used for output dir and files.
######################################################################

ROOT=.
DOMAIN=$1
DOMAIN_DIR=$ROOT/$DOMAIN.ssl
DOMAIN_KEY=$DOMAIN_DIR/$DOMAIN.key
DOMAIN_CSR=$DOMAIN_DIR/$DOMAIN.csr

mkdir $DOMAIN_DIR

if [ ! -f "$DOMAIN_KEY" ]
then
    echo "[Generating Key] :: $DOMAIN_KEY"
    openssl genrsa -out $DOMAIN_KEY 2048
else
    echo "[Key Already Present] :: $DOMAIN_KEY" 
fi

openssl req -text -new -sha256 -key $DOMAIN_KEY -out $DOMAIN_CSR 

crt-echo.sh $DOMAIN_CSR

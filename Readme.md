# csr-tools
*A helper toolkit for csr/crt generation and bundling*

## Overview

**csr-tools** is a collection of scripts I made to automate and explain two tiresome tasks:

* Generating a CSR request (a request for a Certificate Authority to issue an SSL `.crt` certificate file)
* Bundling multiple CRTs (as provided by CA) into a single concatenated file (as required by some servers)

In particular, I targeted Comodo CRT bundles purchased from SSLs.com.  Your mileage may vary.

## Included Scripts

### [install.sh](install.sh)

Installs all of the other scripts using what I believe is the recommended practice.

Installed scripts do not require the `bash` or `chmod` to use, but the `.sh` extension is still required.

### [bash-header.sh](bash-header.sh)

Generates a bash comment style ASCII header with leading and trailing borders based on the input text size.

#### ?Lol Que?

Since bundling the CRTs dumps multiple encrypted files into the same file, it can be hard to keep them straight.

I made this script to clearly indicate when a source file was being appended and what it's name was.

Because file names can be all different lengths, I got irritated that the leading and trailing markers were always 
too long or too short.  I did some research and came up with a repeating character technique so they are always pretty.

#### Example

`bash-header.sh "Hello World"` generates:

```
###############
## Hello World
###############
```

Notice how the lead is always two #s and there is one padding # before and after the length of the text.  Boo...Yah...

### [csr-gen.sh](csr-gen.sh)

This script generates a folder that contains an RSA key and a CSR request file, using OpenSSL (Warning: Dependency).

The only parameter is the name of the domain.  The output will be based on this name.  You still need to input the name manually for the CSR stage.

#### Example

`csr-gen.sh domain.com` will:

* Prompt the user for CSR details

* Generate the following files:

```
./domain.com.ssl/
    /domain.com.key
    /domain.com.csr
```

* Echo the CSR file on the console.

#### Notes

* Because the key is required when installing the resulting CRT on your server, you don't want to lose this by accident.  It is only generated if not present, but the CSR is overwritten on each run.

* CSRs require a bunch of identifying information to create.  OpenSSL will prompt you for all of this info and then generate the files.

* Linux tends to come with a CSR viewer tool, but I found it cumbersome.  The `-text` option is used so the CSR file will include a raw text description of all the fields and they will be displayed when the file is run.  This may be required for some CAs (or if you space out and get worried while your working).

### [crt-echo.sh](crt-echo.sh)

Echos a file with a `bash-header.sh` lead so it's clear where the file starts and what it's original name is.

Used to concatenate CRT files into a bundle.

#### Example

This is a little trippy.  The file can be any file; the script is named as is because I use it for crt bundling purposes.

`crt-echo.sh crt-echo.sh` outputs:

```
#########################
## SOURCE :: crt-echo.sh
#########################
#!/bin/bash

...
```

You can then use `>` or `>>` to write / append this to a file.

### [crt-echo-comodo.sh](crt-echo-comodo.sh)

Uses `crt-echo.sh` to output the chain of trust CRT files for a Comodo SSL certificate in the proper order.

A Comodo SSL bundle comes with several chain of trust files.  The User's browser will complain they are missing, if only the basic CRT file is installed on the server.  

To fix this, you create a concatenated "bundle" file.

This script simply echos the chain of trust files in the the proper order for a bundle file:

* COMODORSADomainValidationSecureServerCA.crt
* COMODORSAAddTrustCA.crt
* AddTrustExternalCARoot.crt

The chain of trust files are always named the same (at least from my experience and research) 

#### NOTES

* The command must be run from the folder containing the chain of trust files.
* The command takes no parameters.
* The basic domain crt is not included in the output.

### [crt-bundle-comodo.sh](crt-bundle-comodo.sh)

Uses `crt-echo.sh` and `crt-echo-comodo.sh` to write a bundled ssl chain of trust file for a specific domain.

Requires the name (no extension) of the Comodo basic domain crt file.

#### Example

If you have a Comodo domain file: `domain_com.crt`

Using: `crt-bundle-comodo.sh domain_com`

* Generates the bundle file: `domain_com.ssl-bundle.crt`
* Echoes the bundle file to the console.

#### NOTES

* The command must be run from the folder containing the chain of trust files.
* The bundle file has an extra header indicating it's a bundle and the original bundle file name.

## Project Background / History

I got started on this project when a buddy asked me to throw up a website for a project he hoped to crowdfund.  

I had seen that Wordpress was a click and go package on OpenShift Online and had great experience with the service for my school portfolios.  I thought I'd try it out.  I set up the service on Openshift, scaffolded out the site structure, and then went to set up a domain alias (something I'd never done before).  

To my chagrin, as soon as the domain was aliased, we started seeing SSL errors.  I hadn't realized that the OpenShift SSL certificate would no longer be valid at a new domain.  It made sense, I just had never considered it.  So we added security exceptions till I got it figured out.  Not a problem for development stage, but not production ready.

So I went to buy an SSL certificate, and after a little research managed to not get fleeced for an ungodly sum (shame on you CAs).  I followed the OpenShift instructions for generating a CSR, got the certificates in an email, but then I had all these extra CRT files.  I had read somewhere I would have to do some junk to put them all together, but I tried just the basic one and it seemed to work fine, so *handwave*.

Then I follow an https link on mobile, and bam, my own site is a possible danger to me (allegedly).  Turns out the security exceptions had obscured the problem with the SSL train of chust (<- I'm keeping this typo), even though some of the visual indicators that remain after an exception had disappeared (i.e. the strike through lock turned green).

So I did some research and found that everybody had different instructions on concatenating the files (and generating the CSRs), and most of them involved copying and pasting and then changing the domain name where applicable, and it was just an ungodly mess of internet, with the most common response being just copy and paste it.

I found one guy with a scriptable form for bundling the Comodo CSRs, but you still had to copy and paste and then change the domain name.  And for who knows what reason, when I tried to make a reusable script, it came out in the wrong order.

I resolved to make something more reusable, and hopefully educational at the same time.

Because my CRT concat script was mysteriously boned, I restarted at the CSR gen.  Easy enough to convert to a script.  I forgot where I sourced the commands, but it's pretty straightforward, and I picked the two command version that was the most readable and scriptable.

Since my CRT concat output was out of order, the next step was to include the file name as a comment.  A little research showed that anything outside the certificate boundaries was okay, so I just threw it in there, but some test examples were ugly and it was hard to spot the file boundaries, so I got obsessive, hence, my fancy header script.

From there, I simply did the CRT echo with header, and then converted my bundling script into separate "Comodo echo" and "Comodo bundle" scripts.

Originally, I used the output file as a parameter, and wrote to the file while also echoing the important output.  I realized this made usage complicated, so I went for a basic echo output format and then relied on piping the output to the file.  This made things way simpler internally, and made usage more coherent.

And then I was done, basically.

I went back and made sure the key generation was conditional, since I had to be careful, and still imagined a scenario where I accidentally reran a terminal history command in haste and deleted my key for an CRT I had just purchased.  

Maybe I'll do some work later to make file paths more flexible; right now you are limited to paths relative to the working directory.

## Author

James Arlow *aka* The Jolly Wizard 

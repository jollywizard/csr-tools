#!/bin/bash

#########################################
## Echoes the contents of a CRT File 
## with the source file name as a header
##
## @param $1 the crt file
#########################################

## Extract the file name (remove leading path)
shortName=${1##*/}

echo  
bash-header.sh "SOURCE :: $shortName"

cat $1

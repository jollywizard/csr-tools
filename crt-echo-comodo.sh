#!/bin/bash

###########################################################
## Uses crt-echo.sh to echo certificate chains from Comodo 
## in an appropriate order for a concatenated crt file.
###########################################################

COMODO_CHAIN="COMODORSADomainValidationSecureServerCA.crt COMODORSAAddTrustCA.crt AddTrustExternalCARoot.crt"
# CITATION: https://gist.github.com/bradmontgomery/6487319

crt-echo.sh COMODORSADomainValidationSecureServerCA.crt
crt-echo.sh COMODORSAAddTrustCA.crt
crt-echo.sh AddTrustExternalCARoot.crt
